const express = require('express'),
app = express(),
bodyParser=require('body-parser'),
PORT = process.env.PORT || 8080;

const animales = ['perro','gato','loro','vaca'];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));

app.get('/', (req, res, next) => {
  res.send(animales);
});

app.get('/animal/:name', (req, res, next) => {
	console.log(req.body.name);
	animales.forEach((item)=>{
		if(item == req.body.name){
			res.send(item);
		}
		else{
			res.status(404).send();
		}
	});
});

app.put('/animal/:name', (req, res, next) => {
	animales.push(req.body.name);
    res.send(req.body.name);
});


app.post('/animal', (req, res, next) => {
	if(req.body.name){
		animales.push(req.body.name);
		res.send(req.body.name);
	}
	else{
		res.send("Debe ingresar el animal");
	}
    
});

app.delete('/animal/:name', (req, res, next) => {
  const indexAnimal = animales.indexOf(req.body.name);
  if (indexAnimal !== -1) {
    animales.splice(indexAnimal, 1);
    res.status(200).send();
  } else {
    res.status(404).send();
  }
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`); 
});



